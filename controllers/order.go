package controllers

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/royhananwar/forex/models"
)

func CreateOrder(c *gin.Context) {
	var input models.CreateOrderInput

	err := c.ShouldBindJSON(&input)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	order := models.Order{
		CurrencyCode: input.CurrencyCode,
		Rate:         input.Rate,
		CreatedAt:    time.Now(),
	}

	models.DB.Create(&order)

	c.JSON(http.StatusCreated, gin.H{"data": &order})
}

func ListOrder(c *gin.Context) {
	var order []models.Order

	models.DB.Find(&order)

	c.JSON(http.StatusOK, gin.H{"data": order})
}
