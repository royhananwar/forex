package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/royhananwar/forex/models"
)

func GetCurrentCurrency(c *gin.Context) {
	var currency models.Currency

	err := models.DB.Where("code = ?", c.Param("code")).First(&currency).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Currency Not Supported"})
		return
	}

	// get the rate from 3rd party
}

func GetSupportedCurrency(c *gin.Context) {
	var currency []models.Currency

	models.DB.Find(&currency)

	c.JSON(http.StatusOK, gin.H{"data": currency})
}
