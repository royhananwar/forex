package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/royhananwar/forex/controllers"
	"gitlab.com/royhananwar/forex/models"
)

func main() {

	server := gin.Default()

	models.ConnectDatabase()

	server.GET("/currency/current/:code", controllers.GetCurrentCurrency)
	server.GET("/currency/support", controllers.GetSupportedCurrency)
	server.GET("/order/list", controllers.ListOrder)
	server.POST("/order/book", controllers.CreateOrder)

	server.Run()
}
