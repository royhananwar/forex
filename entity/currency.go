package entity

type Currency struct {
	Id      string
	Country string
	Code    string
}
