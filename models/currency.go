package models

type Currency struct {
	ID      uint   `json:"id" gorm:"primary_key"`
	Country string `json:"country"`
	Code    string `json:"code"`
}
