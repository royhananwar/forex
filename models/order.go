package models

import "time"

type Order struct {
	ID           uint   `json:"id" gorm:"primary_key"`
	CurrencyCode string `json:"currencyCode"`
	Rate         uint   `json:"rate"`
	CreatedAt    time.Time
}

type CreateOrderInput struct {
	CurrencyCode string `json:"currencyCode" binding:"required"`
	Rate         uint   `json:"rate" binding:"required"`
}
